package primePaths;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class NearestNeighbourAlgorithm {
	private int PATH_LENGTH = Main.NUM_OF_ROWS + 1;

	private double calcTotalDistance(int[] pathOrderOfCityIds) {
		double distance = 0;
		for (int i = 1; i < PATH_LENGTH; i++) {
			int cityIdOnI = pathOrderOfCityIds[i];
			int cityIdOn1I = pathOrderOfCityIds[i - 1];
			distance += Main.calcDistance(cityIdOn1I, cityIdOnI) * coeff(i - 1, cityIdOn1I);
		}
		return distance;
	}

	private double coeff(int indexOfStep, int cityId) {
		if ((indexOfStep % 10 == 9) && (!Main.primeIndexes[cityId]))
			return 1.1;
		return 1.0;
	}

	public int[] calc() {
		int[] visitingOrderOfCityIds = new int[PATH_LENGTH];
		Set<Integer> unvisitedCityIds = new HashSet<Integer>(
				Arrays.asList(Arrays.stream(Main.cityIds).boxed().toArray(Integer[]::new)));

		int currentCityId = 0;
		visitingOrderOfCityIds[0] = currentCityId;
		unvisitedCityIds.remove(currentCityId);

		int stepNumber = 1;
		while (!unvisitedCityIds.isEmpty()) {
			double minDistance = Double.MAX_VALUE;
			int minCityId = 0;

			for (Integer cityIdTemp : unvisitedCityIds) {
				double distanceTemp = Main.calcDistance(currentCityId, cityIdTemp);
				if (distanceTemp < minDistance) {
					minDistance = distanceTemp;
					minCityId = cityIdTemp;
				}
			}

			currentCityId = minCityId;
			visitingOrderOfCityIds[stepNumber] = currentCityId;
			unvisitedCityIds.remove(currentCityId);
			stepNumber++;

		}

		visitingOrderOfCityIds[stepNumber] = 0;

		System.out.println(calcTotalDistance(visitingOrderOfCityIds));

		return visitingOrderOfCityIds;
	}
}
