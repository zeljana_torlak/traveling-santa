package primePaths;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

	public static final int NUM_OF_ROWS = 197768 + 1; //max city id plus one for zero

	private static PrimeCalc primeCalc = new PrimeCalc();
	public static boolean[] primeIndexes;

	private static MapDrawer mapDrawer = new MapDrawer();

	public static int[] cityIds = new int[NUM_OF_ROWS];
	public static double[] Xs = new double[NUM_OF_ROWS];
	public static double[] Ys = new double[NUM_OF_ROWS];

	private static NearestNeighbourAlgorithm NNAlgorithm = new NearestNeighbourAlgorithm();
	private static NearestNeighbourAlgorithmOptimized NNOAlorithm = new NearestNeighbourAlgorithmOptimized();
	private static TwoOptAlgorithm twoOptAlgorithm = new TwoOptAlgorithm();
	private static ThreeOptAlgorithm threeOptAlgorithm = new ThreeOptAlgorithm();
	private static GeneticAlgorithm geneticAlgorithm = new GeneticAlgorithm();

	private static void readInputFromCSV() {
		try (BufferedReader br = new BufferedReader(new FileReader("cities.csv"))) {
			String line;
			line = br.readLine();
			int i = 0;
			while ((line = br.readLine()) != null) {
				String[] values = line.split(",");
				cityIds[i] = Integer.parseInt(values[0]);
				Xs[i] = Double.parseDouble(values[1]);
				Ys[i] = Double.parseDouble(values[2]);
				i++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void writeOutputToCSV(int[] cityIdsOrder, String postfix) {
		try (FileWriter writer = new FileWriter("Submission" + postfix + ".csv");) {
			writer.append("Path\n");
			for (int j = 0; j < cityIdsOrder.length; j++) {
				writer.append(String.valueOf(cityIdsOrder[j]));
				writer.append("\n");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static double calcDistance(int sourceCityId, int targetCityId) { // Euclidean distance
		double x1 = Xs[sourceCityId];
		double y1 = Ys[sourceCityId];
		double x2 = Xs[targetCityId];
		double y2 = Ys[targetCityId];
		return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
	}

	public static void main(String[] args) {
		primeIndexes = primeCalc.calcPrimesBelowNumber(NUM_OF_ROWS);

		readInputFromCSV();

		// mapDrawer.drawMap(Xs, Ys);

		writeOutputToCSV(NNAlgorithm.calc(), "NN");
		// writeOutputToCSV(NNOAlorithm.calc(), "NNO");
		// writeOutputToCSV(twoOptAlgorithm.calc(), "2opt");
		// writeOutputToCSV(threeOptAlgorithm.calc(), "3opt");
		// writeOutputToCSV(geneticAlgorithm.calc(), "GA");

		System.out.println("\nfinished");
	}

}
