package primePaths;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class GeneticAlgorithm {
	private int POPULATION_SIZE = 100; //
	private double CROSSOVER_PROBABILITY = 0.3;
	private double MUTATION_PROBABILITY = 0.05;
	private int PATH_LENGTH = Main.NUM_OF_ROWS + 1;

	private int[][] currentPopulation = new int[POPULATION_SIZE][];
	private double[] currentFitnesses = new double[POPULATION_SIZE];
	private double currentFitnessesSum = 0;

	private int[][] getZeroGenerationPathsFromCSV() {
		int[][] paths = new int[POPULATION_SIZE][];

		int[] temp = new int[PATH_LENGTH];
		try (BufferedReader br = new BufferedReader(new FileReader("results/SubmissionNNO_1.09.csv"))) {
			String line;
			line = br.readLine();
			int i = 0;
			while ((line = br.readLine()) != null) {
				String[] values = line.split(",");
				temp[i] = Integer.parseInt(values[0]);
				i++;
			}
			paths[0] = temp;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (int index = 1; index < POPULATION_SIZE; index++) {
			paths[index] = paths[0].clone();
		}
		return paths;
	}

	private int[][] createRandomZeroGenerationPaths() {
		int[][] paths = new int[POPULATION_SIZE][];

		for (int index = 0; index < POPULATION_SIZE; index++) {
			int[] temp = new int[PATH_LENGTH];
			LinkedList<Integer> unvisitedCityIds = new LinkedList<Integer>(
					Arrays.asList(Arrays.stream(Main.cityIds).boxed().toArray(Integer[]::new)));
			unvisitedCityIds.removeFirst();
			temp[0] = temp[PATH_LENGTH - 1] = 0;
			Collections.shuffle(unvisitedCityIds);

			for (int i = 1; i <= (PATH_LENGTH - 2); i++) {
				temp[i] = unvisitedCityIds.removeFirst();
			}
			paths[index] = temp;
		}
		return paths;
	}

	private double calcTotalDistance(int[] pathOrderOfCityIds) {
		double distance = 0;
		for (int i = 1; i < PATH_LENGTH; i++) {
			int cityIdOnI = pathOrderOfCityIds[i];
			int cityIdOn1I = pathOrderOfCityIds[i - 1];
			distance += Main.calcDistance(cityIdOn1I, cityIdOnI) * coeff(i - 1, cityIdOn1I);
		}
		return distance;
	}

	private double coeff(int indexOfStep, int cityId) {
		if ((indexOfStep % 10 == 9) && (!Main.primeIndexes[cityId]))
			return 1.1;
		return 1.0;
	}

	private int[][] createTwoChildren() {
		int[][] twoChildren = new int[2][];

		int[][] momAndDad = selectParents();

		if (Math.random() < CROSSOVER_PROBABILITY) {
			twoChildren = crossover(momAndDad);
		} else {
			twoChildren[0] = momAndDad[0].clone();
			twoChildren[1] = momAndDad[1].clone();
		}

		twoChildren[0] = mutateIndividual(twoChildren[0]);
		twoChildren[1] = mutateIndividual(twoChildren[1]);

		return twoChildren;
	}

	private int[][] selectParents() {
		int[][] parents = new int[2][];

		boolean momOk = false;
		boolean dadOk = false;
		double momRoll = Math.random() * currentFitnessesSum;
		double dadRoll = Math.random() * currentFitnessesSum;

		for (int i = 0; i < POPULATION_SIZE; i++) {
			double tempFitness = currentFitnesses[i];
			if (!momOk && (momRoll < tempFitness)) {
				momOk = true;
				parents[0] = currentPopulation[i];
				if (momOk && dadOk)
					break;
			}
			if (!dadOk && (dadRoll < tempFitness)) {
				dadOk = true;
				parents[1] = currentPopulation[i];
				if (momOk && dadOk)
					break;
			}
			momRoll -= tempFitness;
			dadRoll -= tempFitness;
		}

		return parents;
	}

	private int[] mutateIndividual(int[] individual) {
		int[] mutatedRoute = individual.clone();

		if (MUTATION_PROBABILITY > 0.0)
			for (int i = 1; i < (PATH_LENGTH - 1); i++) {
				if (Math.random() < MUTATION_PROBABILITY) {
					int randIndex = (int) Math.floor(Math.random() * PATH_LENGTH);
					if (randIndex == 0)
						randIndex = 1;
					if (randIndex >= (PATH_LENGTH - 1))
						randIndex = PATH_LENGTH - 2;
					int tempCityId = mutatedRoute[randIndex];
					mutatedRoute[randIndex] = mutatedRoute[i];
					mutatedRoute[i] = tempCityId;
				}
			}

		return mutatedRoute;
	}

	private int[][] crossover(int[][] momAndDad) {
		int[][] twoChildren = new int[2][];

		int num1 = (int) Math.floor(PATH_LENGTH * Math.random());
		int num2 = (int) Math.floor(PATH_LENGTH * Math.random());
		// obezbediti da indeksi ne diraju nulti grad - pocetni i krajnji
		if (num1 == 0)
			num1 = 1;
		if (num2 == 0)
			num2 = 1;
		if (num1 >= (PATH_LENGTH - 1))
			num1 = PATH_LENGTH - 2;
		if (num2 >= (PATH_LENGTH - 1))
			num2 = PATH_LENGTH - 2;
		int segmentStart = Math.min(num1, num2);
		int segmentEnd = Math.max(num1, num2);

		twoChildren[0] = orderedCrossover(segmentStart, segmentEnd, momAndDad[0], momAndDad[1]);
		twoChildren[1] = orderedCrossover(segmentStart, segmentEnd, momAndDad[1], momAndDad[0]);

		return twoChildren;
	}

	private int[] orderedCrossover(int segmentStart, int segmentEnd, int[] segParent, int[] otherParent) {
//		ArrayList<Integer> child = (ArrayList<Integer>) Arrays.stream(Arrays.copyOfRange(segParent, segmentStart, segmentEnd))
//				.boxed()
//			    .collect(Collectors.toList());

		final List<Integer> child = Collections.synchronizedList((ArrayList<Integer>) Arrays
				.stream(Arrays.copyOfRange(segParent, segmentStart, segmentEnd)).boxed().collect(Collectors.toList()));

		IntStream.range(0, PATH_LENGTH).parallel().forEach(i -> {
			int parentIndex = (segmentEnd + i) % PATH_LENGTH;
			int parentCityId = otherParent[parentIndex];
			if (parentCityId != 0 && !child.contains(parentCityId))
				child.add(parentCityId);
		});
//		for (int i = 0; i < PATH_LENGTH; i++) {
//			int parentIndex = (segmentEnd + i) % PATH_LENGTH;
//			int parentCityId = otherParent[parentIndex];
//			if (parentCityId == 0)
//				continue;
//			
//			if (!child.contains(parentCityId))
//				child.add(parentCityId);
//		}

		// rotate
		int offset = child.size() - segmentStart;
		ArrayList<Integer> child2 = (ArrayList<Integer>) Stream
				.concat(child.stream().skip(offset), child.stream().limit(offset)).collect(Collectors.toList());
		child.clear();

		child2.add(0, 0);
		child2.add(0);
		return child2.stream().mapToInt(Integer::intValue).toArray();
	}

	public int[] calc() { // ne sme da se dira prvi i poslednji cvor
		int[] bestPathEver = new int[PATH_LENGTH];
		double bestPathEverDistance = Double.MAX_VALUE;

		// create Generation 0
		int generationNumber = 0;
		int[] bestPathNow = null;
		double bestPathNowDistance = Double.MAX_VALUE;
		currentFitnessesSum = 0;
		currentPopulation = getZeroGenerationPathsFromCSV(); //createRandomZeroGenerationPaths();
		for (int i = 0; i < POPULATION_SIZE; i++) {
			double tempDistance = calcTotalDistance(currentPopulation[i]);
			currentFitnesses[i] = 1 / tempDistance;
			currentFitnessesSum += 1 / tempDistance;
			if (tempDistance < bestPathNowDistance) {
				bestPathNowDistance = tempDistance;
				bestPathNow = currentPopulation[i];
			}
		}
		bestPathEverDistance = bestPathNowDistance;
		bestPathEver = bestPathNow;
		Main.writeOutputToCSV(bestPathNow, "GAgen" + generationNumber);

		// create next generations
		System.out.println("POPULATION SIZE: " + POPULATION_SIZE + "\tCROSSOVER_PROBABILITY: " + CROSSOVER_PROBABILITY
				+ "\tMUTATION_PROBABILITY: " + MUTATION_PROBABILITY + "\n" + "Generation:" + generationNumber
				+ "\tBEST DISTANCE ever:" + bestPathEverDistance + "\tnow:" + bestPathNowDistance);
		boolean ok = true;
		while (ok) {

			int[][] evolvedPopulation = new int[POPULATION_SIZE][];
			for (int i = 0; i < (POPULATION_SIZE / 2); i++) {
				int[][] twoChildren = createTwoChildren();
				evolvedPopulation[i * 2] = twoChildren[0];
				evolvedPopulation[i * 2 + 1] = twoChildren[1];
			}
			if (POPULATION_SIZE % 2 == 1) {
				evolvedPopulation[POPULATION_SIZE - 1] = createTwoChildren()[0];
			}

			// update current population
			generationNumber++;
			currentPopulation = evolvedPopulation;
			bestPathNow = null;
			bestPathNowDistance = Double.MAX_VALUE;
			currentFitnessesSum = 0;
			for (int i = 0; i < POPULATION_SIZE; i++) {
				double tempDistance = calcTotalDistance(evolvedPopulation[i]);
				currentFitnesses[i] = 1 / tempDistance;
				currentFitnessesSum += 1 / tempDistance;
				if (tempDistance < bestPathNowDistance) {
					bestPathNowDistance = tempDistance;
					bestPathNow = evolvedPopulation[i];
				}
			}
			if (bestPathNowDistance < bestPathEverDistance) {
				bestPathEverDistance = bestPathNowDistance;
				bestPathEver = bestPathNow;
			}

			Main.writeOutputToCSV(bestPathEver, "GA");
			System.out.println("Generation:" + generationNumber + "\t\tBEST DISTANCE ever:" + bestPathEverDistance
					+ "\t\tnow:" + bestPathNowDistance);
		}

		return bestPathEver;
	}
}
