package primePaths;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class ThreeOptAlgorithm {
	private int PATH_LENGTH = Main.NUM_OF_ROWS + 1;
	private int[] visitingOrderOfCityIds = new int[PATH_LENGTH];

	private int[] readNNPathFromCSV() {
		int[] temp = new int[PATH_LENGTH];
		try (BufferedReader br = new BufferedReader(new FileReader("results/Submission2opt_NN_1.09.csv"))) {
			String line;
			line = br.readLine();
			int i = 0;
			while ((line = br.readLine()) != null) {
				String[] values = line.split(",");
				temp[i] = Integer.parseInt(values[0]);
				i++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return temp;
	}

	private int[] readOriginalOrder() {
		int[] temp = new int[PATH_LENGTH];
		temp[0] = temp[PATH_LENGTH - 1] = 0;
		for (int i = 1; i < Main.cityIds.length; i++)
			temp[i] = Main.cityIds[i];
		return temp;
	}

	private double calcTotalDistance() {
		double distance = 0;
		for (int i = 1; i < PATH_LENGTH; i++) {
			int cityIdOnI = visitingOrderOfCityIds[i];
			int cityIdOn1I = visitingOrderOfCityIds[i - 1];
			distance += Main.calcDistance(cityIdOn1I, cityIdOnI) * coeff(i - 1, cityIdOn1I);
		}
		return distance;
	}

	private double coeff(int indexOfStep, int cityId) {
		if ((indexOfStep % 10 == 9) && (!Main.primeIndexes[cityId]))
			return 1.1;
		return 1.0;
	}

	private void swap(int i, int j) {
		for (int k = i + 1; k < ((j + i + 2) / 2); k++) {// elementi od i+1 do j se rotiraju
			int temp = visitingOrderOfCityIds[k];
			visitingOrderOfCityIds[k] = visitingOrderOfCityIds[j + i + 1 - k];
			visitingOrderOfCityIds[j + i + 1 - k] = temp;
		}
	}

	private double reverse_segment_if_better(int i, int j, int k) {
		int A = visitingOrderOfCityIds[i - 1];
		int B = visitingOrderOfCityIds[i];
		int C = visitingOrderOfCityIds[j - 1];
		int D = visitingOrderOfCityIds[j];
		int E = visitingOrderOfCityIds[k - 1];
		int F = visitingOrderOfCityIds[k];

		double d0 = Main.calcDistance(A, B) * coeff(i - 1, A) + Main.calcDistance(C, D) * coeff(j - 1, C)
				+ Main.calcDistance(E, F) * coeff(k - 1, E);
		double d1 = Main.calcDistance(A, C) * coeff(i - 1, A) + Main.calcDistance(B, D) * coeff(j - 1, B)
				+ Main.calcDistance(E, F) * coeff(k - 1, E);
		double d2 = Main.calcDistance(A, B) * coeff(i - 1, A) + Main.calcDistance(C, E) * coeff(j - 1, C)
				+ Main.calcDistance(D, F) * coeff(k - 1, D);
		double d3 = Main.calcDistance(A, D) * coeff(i - 1, A) + Main.calcDistance(E, B) * coeff(j - 1, E)
				+ Main.calcDistance(C, F) * coeff(k - 1, C);
		double d4 = Main.calcDistance(F, B) * coeff(i - 1, F) + Main.calcDistance(C, D) * coeff(j - 1, C)
				+ Main.calcDistance(E, A) * coeff(k - 1, E);

		if (d0 > d1) {
			swap(i - 1, j);
			return d1 - d0;
		} else if (d0 > d2) {
			swap(j - 1, k);
			return d2 - d0;
		} else if (d0 > d4) {
			swap(i - 1, k);
			return d4 - d0;
		} else if (d0 > d3) {
			int[] temp1 = Arrays.copyOfRange(visitingOrderOfCityIds, j, k);
			int[] temp2 = Arrays.copyOfRange(visitingOrderOfCityIds, i, j);

			int index = i;
			for (int temp : temp1) {
				visitingOrderOfCityIds[index] = temp;
				index++;
			}
			for (int temp : temp2) {
				visitingOrderOfCityIds[index] = temp;
				index++;
			}

			return d3 - d0;
		}

		return 0.0;
	}

	public int[] calc() { // ne sme da se dira prvi i poslednji cvor
		visitingOrderOfCityIds = readNNPathFromCSV();
		// visitingOrderOfCityIds = readOriginalOrder();

		int[] bestPathEver = visitingOrderOfCityIds.clone();
		double bestDistance = calcTotalDistance();

		System.out.println(bestDistance);
		while (true) {
			double delta = 0;

			for (int a = 2; a < PATH_LENGTH - 6; a++)
				for (int b = a + 2; b < PATH_LENGTH - 4; b++)
					for (int c = b + 2; c < PATH_LENGTH - 2; c++)
						delta += reverse_segment_if_better(a, b, c);

			double currentDisance = calcTotalDistance();
			if (currentDisance < bestDistance) {
				bestDistance = currentDisance;
				bestPathEver = visitingOrderOfCityIds.clone();
			}
			Main.writeOutputToCSV(bestPathEver, "3opt");
			System.out.println(currentDisance);
			if (delta >= 0)
				break;
		}

		return visitingOrderOfCityIds;
	}
}
