package primePaths;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class MapDrawer {

	public void drawMap(double[] Xs, double[] Ys) {
		final BufferedImage image = new BufferedImage(5100, 3400, BufferedImage.TYPE_INT_ARGB);
		final Graphics2D graphics2D = image.createGraphics();

		graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
		graphics2D.setPaint(Color.WHITE);
		graphics2D.fillRect(0, 0, 5100, 3400);
		graphics2D.setPaint(Color.BLACK);
		for (int i = 0; i < Xs.length; i++)
			graphics2D.draw(new Rectangle2D.Double(Xs[i], Ys[i], 2, 2));
		graphics2D.dispose();

		try {
			ImageIO.write(image, "png", new File("image.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
