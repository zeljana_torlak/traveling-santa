package primePaths;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class TwoOptAlgorithm {
	private int PATH_LENGTH = Main.NUM_OF_ROWS + 1;
	private int[] visitingOrderOfCityIds = new int[PATH_LENGTH];

	private int[] readNNPathFromCSV() {
		int[] temp = new int[PATH_LENGTH];
		try (BufferedReader br = new BufferedReader(new FileReader("results/SubmissionNNO_1.09.csv"))) {
			String line;
			line = br.readLine();
			int i = 0;
			while ((line = br.readLine()) != null) {
				String[] values = line.split(",");
				temp[i] = Integer.parseInt(values[0]);
				i++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return temp;
	}

	private int[] readOriginalOrder() {
		int[] temp = new int[PATH_LENGTH];
		temp[0] = temp[PATH_LENGTH - 1] = 0;
		for (int i = 1; i < Main.cityIds.length; i++)
			temp[i] = Main.cityIds[i];
		return temp;
	}

	private double calcTotalDistance() {
		double distance = 0;
		for (int i = 1; i < PATH_LENGTH; i++) {
			int cityIdOnI = visitingOrderOfCityIds[i];
			int cityIdOn1I = visitingOrderOfCityIds[i - 1];
			distance += Main.calcDistance(cityIdOn1I, cityIdOnI) * coeff(i - 1, cityIdOn1I);
		}
		return distance;
	}

	private double coeff(int indexOfStep, int cityId) {
		if ((indexOfStep % 10 == 9) && (!Main.primeIndexes[cityId]))
			return 1.1;
		return 1.0;
	}

	private boolean checkSwap(int pos1, int pos2) {
		int cityIdOnPos1 = visitingOrderOfCityIds[pos1];
		int cityIdOnPos11 = visitingOrderOfCityIds[pos1 + 1];
		int cityIdOnPos2 = visitingOrderOfCityIds[pos2];
		int cityIdOnPos21 = visitingOrderOfCityIds[pos2 + 1];

		double existingDistance = Main.calcDistance(cityIdOnPos1, cityIdOnPos11) * coeff(pos1, cityIdOnPos1)
				+ Main.calcDistance(cityIdOnPos2, cityIdOnPos21) * coeff(pos2, cityIdOnPos2);

		double newDistance = Main.calcDistance(cityIdOnPos1, cityIdOnPos2) * coeff(pos1, cityIdOnPos1)
				+ Main.calcDistance(cityIdOnPos11, cityIdOnPos21) * coeff(pos2, cityIdOnPos11);

		return newDistance < existingDistance;
	}

	private void swap(int i, int j) {
		for (int k = i + 1; k < ((j + i + 2) / 2); k++) {// elementi od i+1 do j se rotiraju
			int temp = visitingOrderOfCityIds[k];
			visitingOrderOfCityIds[k] = visitingOrderOfCityIds[j + i + 1 - k];
			visitingOrderOfCityIds[j + i + 1 - k] = temp;
		}
	}

	public int[] calc() { // ne sme da se dira prvi i poslednji cvor
		visitingOrderOfCityIds = readNNPathFromCSV();
		// visitingOrderOfCityIds = readOriginalOrder();

		System.out.println(calcTotalDistance());
		while (true) {
			boolean ok = true;
			for (int i = 0; i < (PATH_LENGTH - 2); i++) {
				for (int j = i + 2; j < (PATH_LENGTH - 1); j++) {
					if (checkSwap(i, j)) {
						swap(i, j);
						ok = false;
					}
				}
			}

			Main.writeOutputToCSV(visitingOrderOfCityIds, "");
			System.out.println(calcTotalDistance());
			if (ok)
				break;
		}

		return visitingOrderOfCityIds;
	}
}
