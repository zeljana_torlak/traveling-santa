package primePaths;

public class PrimeCalc {

	public boolean[] calcPrimesBelowNumber(int max) {
		// Sieve of Eratosthenes
		boolean[] index = new boolean[max];

		index[0] = index[1] = false;
		for (int i = 2; i < index.length; i++)
			index[i] = true;

		int sqrtOfMax = (int) Math.sqrt(max);

		for (int i = 2; i <= sqrtOfMax; i++) {
			if (index[i] == true) {
				for (int j = (int) Math.pow(i, 2); j < max; j += i)
					index[j] = false;
			}
		}

		return index;
	}

}
